const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");
const { VueLoaderPlugin } = require("vue-loader");
const HtmlWebPackPlugin = require("html-webpack-plugin");

module.exports = {
  entry: "./src/index",
  output: {
    publicPath: "http://localhost:7000/",
  },

  resolve: {
    extensions: [".vue", ".js", ".json"],
  },

  devServer: {
    port: 7000,
    historyApiFallback: true,
  },

  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: "vue-loader",
      },
      {
        test: /\.(css|s[ac]ss)$/i,
        use: ["style-loader", "css-loader", "postcss-loader"],
      },
    ],
  },

  plugins: [
    new VueLoaderPlugin(),
    new ModuleFederationPlugin({
      name: "test",
      filename: "remoteEntry.js",
      remotes: {},
      exposes: {},
      shared: [],
    }),
    new HtmlWebPackPlugin({
      template: "./src/index.html",
    }),
  ],
};
