import { createRouter, createWebHistory } from 'vue-router';
import NotFound from '../components/NotFound.vue';
import home from './components-routes/Home.js';
import Header from './components-routes/Header.js';
import listLike from './components-routes/ListLike.js';
import setting from './components-routes/Setting';

const routes = [
  home,
  listLike,
  setting,

  Header,
  {
    name: 'notFound',
    path: '/:pathMatch(.*)',
    component: NotFound,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.afterEach((to) => {
  document.title = to.meta.title;
});

export default router;
