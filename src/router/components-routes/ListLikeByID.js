import LikeByID from '../../views/LikeByIDView.vue';

const listLikeByID = {
  name: 'listLikeByID',
  path: ':id',
  component: LikeByID,
  meta: {
    title: 'LIKE BY ID',
  },
  props: true,
};

export default listLikeByID;
