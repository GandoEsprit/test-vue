import ConnectView from '../../views/ConnectView.vue';
import store from '../../store';
import { MutationsUser } from '../../store/auth/auth.type';

const home = {
  name: 'connection',
  path: '/',
  component: ConnectView,
  meta: {
    title: 'CONNECTION',
  },
  beforeEnter: () => {
    store.commit(MutationsUser.SET_USER, []);
  },
};
export default home;
