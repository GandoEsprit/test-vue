import SettingView from '../../views/SettingView.vue';
import store from '../../store/index.js';
import { computed } from 'vue';

const setting = {
  name: 'setting',
  path: '/setting',
  component: SettingView,
  meta: {
    title: 'SETTING',
  },
  beforeEnter: (to, from, next) => {
    const user = computed(() => store.getters.user);
    if (user.value.length > 0) {
      next();
    } else {
      return next({ path: '/', replace: true });
    }
  },
};

export default setting;
