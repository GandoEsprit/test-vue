import ListLike from '../../views/ListLikeView.vue';

const listLike = {
  name: 'listLike',
  path: '/listLike',
  component: ListLike,
  meta: {
    title: 'LIST LIKE',
  },

  props: true,
};

export default listLike;
