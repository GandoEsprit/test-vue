import HeaderView from '../../views/HeaderView.vue';
import store from '../../store/index.js';
import { computed } from 'vue';

const Header = {
  name: 'header',
  path: '/header',
  component: HeaderView,
  meta: {
    title: 'HOME PAGE',
  },
  beforeEnter: (to, from, next) => {
    const user = computed(() => store.getters.user);
    if (user.value.length > 0) {
      next();
    } else {
      return next({ path: '/', replace: true });
    }
  },
};

export default Header;
