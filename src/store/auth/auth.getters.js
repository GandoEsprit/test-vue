export default {
  user: (state) => state.user,
  userNameBool: (state) => state.userNameBool,
  userEmailBool: (state) => state.userEmailBool,
  userPasswordBool: (state) => state.userPasswordBool,
};
