import { MutationsUser } from './auth.type';
export default {
  [MutationsUser.SET_USER]: (state, payload) => (state.user = payload),
  [MutationsUser.SET_USER_NAME_BOOL]: (state, payload) => (state.userNameBool = payload),
  [MutationsUser.SET_USER_EMAIL_BOOL]: (state, payload) => (state.userEmailBool = payload),
  [MutationsUser.SET_USER_PASSWORD_BOOL]: (state, payload) => (state.userPasswordBool = payload),
};
