import { ActionsUser, MutationsUser } from './auth.type';
import ServiceLog from '../../service/auth.service';

export default {
  async [ActionsUser.GET_USER]({ commit }, data) {
    const response = await ServiceLog.getUserByNameAndPassword(data.name, data.password);
    if (response.data.length <= 0) {
      throw 'Error: name or password does not exist';
    }
    commit(MutationsUser.SET_USER, response.data);
  },
  async [ActionsUser.ADD_USER]({ commit }, data) {
    const existUserName = await ServiceLog.existUserName(data.name);
    const existEmail = await ServiceLog.existEmail(data.email);
    if (existUserName) {
      throw 'The user name is alreadi exist';
    }
    if (existEmail) {
      throw 'The email is alreadi exist';
    }
    await ServiceLog.postUserData(data);
    const response = await ServiceLog.getUserByName(data.name);
    commit(MutationsUser.SET_USER, response.data);
  },

  async [ActionsUser.PUT_NAME]({ commit }, data) {
    if (data.name === '' || data.password === '' || data.oldName === '') {
      throw 'The data value info is null';
    }

    const existNewName = await ServiceLog.existUserName(data.name);

    if (existNewName) {
      throw 'The name that you want add exist';
    }

    const existPassword = await ServiceLog.existPassword(data.oldName, data.password);

    if (!existPassword) {
      throw 'The password does not exist';
    }

    await ServiceLog.putUser(data);

    const response = await ServiceLog.getUserByName(data.name);
    commit(MutationsUser.SET_USER, response.data);
  },

  async [ActionsUser.PUT_EMAIL]({ commit }, data) {
    if (data.name === '' || data.email === '' || data.password === '' || data.id === '') {
      throw 'The data values is null';
    }

    const existNewEmail = await ServiceLog.existEmail(data.email);

    if (existNewEmail) {
      throw 'The new email is already exist';
    }

    const existPassword = await ServiceLog.existPassword(data.name, data.password);

    if (!existPassword) {
      throw 'The password does not exist';
    }

    await ServiceLog.putUser(data);

    const response = await ServiceLog.getUserByEmail(data.newEmail);

    commit(MutationsUser.SET_USER, response.data);
  },

  async [ActionsUser.PUT_PASSWORD]({ commit }, data) {
    if (data.id === '' || data.oldPassword === '' || data.newPassword === '' || data.name === '' || data.email === '') {
      throw 'Data value is error';
    }

    const existOldPassword = await ServiceLog.existPassword(data.name, data.oldPassword);

    console.log(data);
    if (!existOldPassword) {
      throw 'The old password does not exist';
    }

    if (data.newPassword === data.oldPassword) {
      throw 'The new password is equal at old password';
    }

    const putData = {
      name: data.name,
      email: data.email,
      id: data.id,
      password: data.newPassword,
    };

    await ServiceLog.putUser(putData);
    commit(MutationsUser.SET_USER, []);
  },

  [ActionsUser.GET_USER_NAME_BOOL]({ commit }, data) {
    commit(MutationsUser.SET_USER_NAME_BOOL, data);
  },

  [ActionsUser.GET_USER_EMAIL_BOOL]({ commit }, data) {
    commit(MutationsUser.SET_USER_EMAIL_BOOL, data);
  },

  [ActionsUser.GET_USER_PASSWORD_BOOL]({ commit }, data) {
    commit(MutationsUser.SET_USER_PASSWORD_BOOL, data);
  },
};
