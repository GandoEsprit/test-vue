export const ActionsUser = {
  ADD_USER: 'addUser',
  GET_USER: 'getUser',
  GET_USER_NAME_BOOL: 'getUserNameBool',
  GET_USER_EMAIL_BOOL: 'getUserEmailBool',
  GET_USER_PASSWORD_BOOL: 'getUserPasswordBool',
  PUT_NAME: 'putName',
  PUT_EMAIL: 'putEmail',
  PUT_PASSWORD: 'putPassword',
};
export const MutationsUser = {
  SET_USER: 'setUser',
  SET_USER_NAME_BOOL: 'setUserName',
  SET_USER_EMAIL_BOOL: 'setUserEmail',
  SET_USER_PASSWORD_BOOL: 'setUserPassword',
};
