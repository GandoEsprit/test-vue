import { createStore } from 'vuex';
import logModule from './auth/auth.module';
import jockerModule from './jocker/jocker.module';

const store = createStore({
  state: {},
  mutations: {},
  actions: {},
  modules: { logModule, jockerModule },
  strict: true,
});

global.store = store;

export default store;
