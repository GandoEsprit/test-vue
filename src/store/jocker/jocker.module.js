import state from './jocker.state';
import actions from './jocker.actions';
import mutations from './jocker.mutations';
import getters from './jocker.getters';

export default {
  state,
  actions,
  mutations,
  getters,
};
