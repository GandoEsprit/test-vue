import { Mutations } from './jocker.type';
const mutations = {
  [Mutations.SET_LIST_LIKE]: (state, payload) => (state.listLike = payload),
  [Mutations.SET_LIST_NO_LIKE]: (state, payload) => (state.listNoLike = payload),
  [Mutations.GET_JOCKER]: (state, payload) => (state.jokerData = payload),
  [Mutations.SET_LIST_LIKE_CLICK]: (state, payload) => (state.listLikeClick = payload),
};

export default mutations;
