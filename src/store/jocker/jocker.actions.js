import { Actions, Mutations } from './jocker.type';
import ServiceRandom from '../../service/jocker.service';

const actions = {
  async [Actions.GET_JOCKER]({ commit }) {
    const response = await ServiceRandom.getJocker();
    commit(Mutations.GET_JOCKER, response.data);
  },

  async [Actions.ADD_LIST_NO_LIKE]({ dispatch }, data) {
    if (data === {}) {
      throw 'Error: Jocker is nil';
    }
    await ServiceRandom.postNoLike(data);
    dispatch(Actions.GET_LIST_NO_LIKE, data.user_id);
    dispatch(Actions.GET_JOCKER);
  },

  async [Actions.ADD_LIST_lIKE]({ dispatch }, data) {
    if (data === {}) {
      throw 'Error: Jocker is nil';
    }
    await ServiceRandom.postLike(data);
    dispatch(Actions.GET_LIST_LIKE, data.user_id);
    dispatch(Actions.GET_JOCKER);
  },
  async [Actions.GET_LIST_LIKE]({ commit }, idUser) {
    const response = await ServiceRandom.getListLike(idUser);
    commit(Mutations.SET_LIST_LIKE, response.data);
  },
  async [Actions.GET_LIST_NO_LIKE]({ commit }, idUser) {
    const response = await ServiceRandom.getListNoLike(idUser);
    commit(Mutations.SET_LIST_NO_LIKE, response.data);
  },
  async [Actions.GET_LIKE_BY_ID]({ commit }, id) {
    const response = await ServiceRandom.getLikeById(id);
    if (response.data.length <= 0) {
      throw 'ID not found';
    }
    commit(Mutations.SET_LIKE_BY_ID, response.data);
  },
  async [Actions.GET_NO_LIKE_BY_ID]({ commit }, id) {
    const response = await ServiceRandom.getNoLikeById(id);
    if (response.data.length <= 0) {
      throw 'ID not found';
    }
    commit(Mutations.SET_NO_LIKE_BY_ID, response.data);
  },
  [Actions.GET_LIST_LIKE_CLICK]({ commit }, value) {
    commit(Mutations.SET_LIST_LIKE_CLICK, value);
  },
};

export default actions;
