export const Actions = {
  ADD_LIST_lIKE: 'addListLike',
  ADD_LIST_NO_LIKE: 'addListNoLike',
  GET_LIST_LIKE: 'getListLike',
  GET_LIST_NO_LIKE: 'getListNoLike',
  GET_JOCKER: 'getJocker',
  GET_LIST_LIKE_CLICK: 'getListLikeClick',
};

export const Mutations = {
  SET_LIST_LIKE: 'setListLike',
  SET_LIST_NO_LIKE: 'setListNoLike',
  SET_NO_LIKE_SERVICE: 'setNoLikeService',
  GET_JOCKER: 'getJocker',
  SET_LIST_LIKE_CLICK: 'setListLikeClick',
};
