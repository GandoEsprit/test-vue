const getters = {
  listLike: (state) => state.listLike,
  listNoLike: (state) => state.listNoLike,
  jocker: (state) => state.jokerData,
  listLikeClick: (state) => state.listLikeClick,
};

export default getters;
