import { defineRule } from 'vee-validate';

defineRule('email', (value) => {
  if (!value || !value.length) {
    return 'The e-mail is nil';
  }

  if (!/[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/.test(value)) {
    return 'The value not is a email';
  }
  return true;
});

defineRule('required', (value) => {
  if (!value || !value.length) {
    return 'The field is required';
  }
  return true;
});

defineRule('alpha', (value, [limit]) => {
  if (value.length < limit) {
    return `The user name should least ${limit}`;
  }

  return true;
});

defineRule('confirmed', (value, [target]) => {
  if (value === target) {
    return true;
  }

  return 'Password is not confirmed';
});
