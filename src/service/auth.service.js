import axios from 'axios';
export default {
  async postUserData(data) {
    await axios.post('http://localhost:3000/userInfo', data);
  },

  async getUserByNameAndPassword(name, password) {
    return await axios.get('http://localhost:3000/userInfo', {
      params: {
        name: name,
        password: password,
      },
    });
  },

  async putUser(data) {
    await axios.put('http://localhost:3000/userInfo/' + data.id, {
      name: data.name,
      email: data.email,
      password: data.password,
    });
  },

  async existUserName(name) {
    const response = await axios.get('http://localhost:3000/userInfo', {
      params: {
        name: name,
      },
    });

    return response.data.length > 0 ? true : false;
  },

  async existEmail(email) {
    const response = await axios.get('http://localhost:3000/userInfo', {
      params: {
        email: email,
      },
    });

    return response.data.length > 0 ? true : false;
  },

  async existPassword(name, password) {
    const response = await axios.get('http://localhost:3000/userInfo', {
      params: {
        name: name,
        password: password,
      },
    });

    return response.data.length > 0 ? true : false;
  },

  async getUserByName(name) {
    const response = await axios.get('http://localhost:3000/userInfo', {
      params: {
        name: name,
      },
    });

    return response;
  },
};
