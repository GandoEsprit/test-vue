import axios from 'axios';

const ServiceRandom = {
  async getJocker() {
    return await axios.get('https://api.chucknorris.io/jokes/random');
  },
  async getListLike(idUser) {
    return await axios.get('http://localhost:3000/listLike', {
      params: { user_id: idUser },
    });
  },
  async getLikeById(id) {
    return await axios.get('http://localhost:3000/listLike', {
      params: {
        id: id,
      },
    });
  },
  async postLike(data) {
    await axios.post('http://localhost:3000/listLike', data);
  },
  async getListNoLike(idUser) {
    return await axios.get('http://localhost:3000/listNoLike', {
      params: {
        user_id: idUser,
      },
    });
  },
  async getNoLikeById(id) {
    return await axios.get('http://localhost:3000/listNoLike', {
      params: {
        id: id,
      },
    });
  },
  async postNoLike(data) {
    await axios.post('http://localhost:3000/listNoLike', data);
  },
};

export default ServiceRandom;
