import axios from 'axios';

const ServiceUserLike = {
  async postUserLike(data) {
    await axios.post('http://localhost:3000/userLike', data);
  },

  async getListUserLike(idUser) {
    return await axios.get('http://localhost:3000/userLike', {
      params: { id_user: idUser },
    });
  },

  async postUserNoLike(data) {
    await axios.post('http://localhost:3000/userNoLike', data);
  },

  async getListUserNolike(idUser) {
    return await axios.post('http://localhost:3000/userNoLike', {
      params: { id_user: idUser },
    });
  },
};
export default ServiceUserLike;
